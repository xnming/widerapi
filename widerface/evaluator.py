'''
The evaluator function for WIDER FACE, this tool is the reimplementation of the
offical MATLAB version
'''

import os
import pickle
import datetime
import argparse
import numpy as np
import scipy.io as sio
import collections

def tprint(message):
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S,%f')
    print('{} {}'.format(timestamp, message))

def load_gt(split):
    dir_name = os.path.dirname(__file__)
    mat = sio.loadmat(os.path.join(dir_name, 'groundtruth/wider_%s_val.mat'%(split)))
    file_list = mat['file_list']
    gt_list = mat.get('gt_list', [])
    face_bbx_list = mat['face_bbx_list']
    event_list = [t[0][0] for t in mat['event_list']]

    gt = {}
    for event_id in range(len(event_list)):
        event_name = event_list[event_id]
        gt[event_name] = {}

        for file_id in range(file_list[event_id][0].shape[0]):
            file_name = file_list[event_id][0][file_id][0][0]
            bbx_gts = face_bbx_list[event_id][0][file_id][0]

            if len(gt_list) > 0:
                vld_gts = np.zeros((bbx_gts.shape[0], 1), dtype=np.float64)
                sub_gt_list = gt_list[event_id][0][file_id][0]
                if sub_gt_list.size > 0:
                    vld_gts[sub_gt_list - 1] = 1
            else:
                vld_gts = np.ones((bbx_gts.shape[0], 1), dtype=np.float64)

            gt[event_name][file_name] = np.hstack((bbx_gts.astype(np.float64), vld_gts))

    return gt

def voc_ap(pr_pair):
    # sort these samples by recall
    pr_pair_t = sorted(pr_pair, key=lambda x : x[1])

    # add extra point (0,0) to avoid incomplete curve
    mpre = [0] + [p[0] for p in pr_pair_t]
    mrec = [0] + [p[1] for p in pr_pair_t]

    ap = 0
    for i in range(1, len(mpre)):
        ap += max(mpre[i - 1], mpre[i]) * (mrec[i] - mrec[i - 1])
    return ap

def bbx_assignment(bbx_pps, bbx_gts, use_official_eval=False):
    # 'reshape' is for broadcast operation
    bbx_ovp_x1 = np.maximum(bbx_pps[:, 0].reshape(-1, 1), bbx_gts[:, 0].reshape(1, -1))
    bbx_ovp_y1 = np.maximum(bbx_pps[:, 1].reshape(-1, 1), bbx_gts[:, 1].reshape(1, -1))
    bbx_ovp_x2 = np.minimum(bbx_pps[:, 2].reshape(-1, 1), bbx_gts[:, 2].reshape(1, -1))
    bbx_ovp_y2 = np.minimum(bbx_pps[:, 3].reshape(-1, 1), bbx_gts[:, 3].reshape(1, -1))

    if not use_official_eval:
        bbx_ovp_dx = (bbx_ovp_x2 - bbx_ovp_x1).clip(0)
        bbx_ovp_dy = (bbx_ovp_y2 - bbx_ovp_y1).clip(0)

        bbx_pps_area = ((bbx_pps[:, 3] - bbx_pps[:, 1]) * \
                        (bbx_pps[:, 2] - bbx_pps[:, 0])).reshape(-1, 1)
        bbx_gts_area = ((bbx_gts[:, 3] - bbx_gts[:, 1]) * \
                        (bbx_gts[:, 2] - bbx_gts[:, 0])).reshape(1, -1)
    else:
        bbx_ovp_dx = (bbx_ovp_x2 - bbx_ovp_x1 + 1).clip(0)
        bbx_ovp_dy = (bbx_ovp_y2 - bbx_ovp_y1 + 1).clip(0)

        bbx_pps_area = ((bbx_pps[:, 3] - bbx_pps[:, 1] + 1) * \
                        (bbx_pps[:, 2] - bbx_pps[:, 0] + 1)).reshape(-1, 1)
        bbx_gts_area = ((bbx_gts[:, 3] - bbx_gts[:, 1] + 1) * \
                        (bbx_gts[:, 2] - bbx_gts[:, 0] + 1)).reshape(1, -1)

    bbx_ovp_area = bbx_ovp_dx * bbx_ovp_dy
    ious = bbx_ovp_area / (bbx_pps_area + bbx_gts_area - bbx_ovp_area)

    max_ind_gts = np.argmax(ious, axis=1)
    max_val_gts = ious[np.arange(ious.shape[0]), max_ind_gts]
    return max_ind_gts, max_val_gts

def bbx_matching(flg_vld_gts, max_ind_gts, max_val_gts, iou_thr):
    num_bbx_gts = flg_vld_gts.shape[0]
    num_bbx_pps = max_ind_gts.shape[0]

    acq_gts = np.empty((num_bbx_gts), dtype=np.int32)
    acq_gts.fill(-1)

    labels = np.empty((num_bbx_pps), dtype=np.int32)
    labels.fill(-1)

    for ind_pp in range(num_bbx_pps):
        ind_gt = max_ind_gts[ind_pp]

        # gt is valid
        if flg_vld_gts[ind_gt] > 0:

            # gt is not acquired and the maximum IoU exceed threshold
            if acq_gts[ind_gt] < 0 and max_val_gts[ind_pp] >= iou_thr:
                labels[ind_pp] = 1
                acq_gts[ind_gt] = 1
            else:
                labels[ind_pp] = 0
        else:
            if max_val_gts[ind_pp] < iou_thr:
                labels[ind_pp] = 0

    return labels

def evaluate(submission_dir, num_pr_bins=1000, use_official_eval=False, verbose=False,
    iou_thrs=[0.5 + 0.05 * i for i in range(10)], splits=['easy', 'medium', 'hard', 'face']):

    if not use_official_eval:
        score_thrs = [1 - float(k) / float(num_pr_bins - 1) for k in range(num_pr_bins)][::-1]
    else:
        score_thrs = [1 - float(k + 1) / float(num_pr_bins) for k in range(num_pr_bins)][::-1]

    # read submissions and normalize scores
    score_min = np.inf
    score_max = -np.inf
    submissions = {}
    for event_name in  sorted(os.listdir(submission_dir)):
        if verbose:
            tprint('Loading event %s...'%(event_name))

        event_dir = os.path.join(submission_dir, event_name)
        if os.path.isdir(event_dir):
            submissions[event_name] = {}
            for file_name in sorted(os.listdir(event_dir)):
                if file_name.endswith('.txt'):
                    with open(os.path.join(event_dir, file_name), 'r') as reader:
                        lines = reader.readlines()

                    file_key = os.path.splitext(os.path.basename(lines[0].strip()))[0]

                    bbx_pps = []
                    for r in range(int(lines[1].strip())):
                        bbx_pps.append([float(t) for t in lines[r + 2].strip().split()])

                    if bbx_pps:
                        bbx_pps = np.array(bbx_pps, dtype=np.float64)
                        bbx_pps[:, 2] = bbx_pps[:, 0] + bbx_pps[:, 2]
                        bbx_pps[:, 3] = bbx_pps[:, 1] + bbx_pps[:, 3]

                        submissions[event_name][file_key] = bbx_pps
                        score_min_t = np.min(bbx_pps[:, -1])
                        if score_min > score_min_t:
                            score_min = score_min_t

                        score_max_t = np.max(bbx_pps[:, -1])
                        if score_max < score_max_t:
                            score_max = score_max_t
                    else:
                        submissions[event_name][file_key] = np.zeros((0, 5), dtype=np.float64)

    for event_name in submissions:
        for file_key in submissions[event_name]:
            scores_t = submissions[event_name][file_key][:, -1]
            submissions[event_name][file_key][:, -1] = (scores_t - score_min) / (score_max - score_min)

    # PR calculation
    pr_info = collections.OrderedDict()
    for split in splits:
        pr_info[split] = collections.OrderedDict()

        groundtruth = load_gt(split)
        pr_pool = {}
        num_vld_gts = 0

        for event_name in submissions:
            if verbose:
                tprint('Evaluating %s/%s...'%(split, event_name))

            for file_key in submissions[event_name]:
                bbx_pps = submissions[event_name][file_key]

                bbx_gts = groundtruth[event_name][file_key]
                bbx_gts = bbx_gts[np.logical_and(bbx_gts[:, 2] > 0, bbx_gts[:, 3] > 0), :]
                if bbx_gts.shape[0] <= 0:
                    continue

                bbx_gts[:, 2] = bbx_gts[:, 0] + bbx_gts[:, 2]
                bbx_gts[:, 3] = bbx_gts[:, 1] + bbx_gts[:, 3]

                num_vld_gts += np.count_nonzero(bbx_gts[:, -1] > 0)

                max_ind_gts, max_val_gts = bbx_assignment(bbx_pps, bbx_gts, use_official_eval)
                flg_vld_gts = bbx_gts[:, -1]
                scores_t = bbx_pps[:, -1]

                for iou_thr in iou_thrs:
                    labels_t = bbx_matching(flg_vld_gts, max_ind_gts, max_val_gts, iou_thr)

                    pr_pair = []
                    for score_thr in score_thrs:
                        masks_t = scores_t >= score_thr
                        num_vld_pps = np.count_nonzero(labels_t[masks_t] >= 0)
                        num_pos_pps = np.count_nonzero(labels_t[masks_t] == 1)
                        pr_pair.append((num_pos_pps, num_vld_pps))
                    pr_pool.setdefault(iou_thr, []).append(pr_pair)

        for iou_thr in iou_thrs:
            pr_pair = []
            for ind_bin in range(num_pr_bins):
                num_pos_pps = sum([p[ind_bin][0] for p in pr_pool[iou_thr]])
                num_vld_pps = sum([p[ind_bin][1] for p in pr_pool[iou_thr]])

                if num_vld_pps > 0:
                    pr_pair.append((
                        float(num_pos_pps) / float(num_vld_pps),
                        float(num_pos_pps) / float(num_vld_gts)))

            pr_info[split][iou_thr] = {}
            pr_info[split][iou_thr]['samples'] = pr_pair
            pr_info[split][iou_thr]['ap'] = voc_ap(pr_pair)

    return pr_info

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    plt.style.use('seaborn-whitegrid')
    # plt.rc('text', usetex=True)

    # parse the arguments
    parser = argparse.ArgumentParser(description='WIDER FACE Evaluation Tool')
    parser.add_argument('-submission_dir', help='directory to store the submissions', type=str, required=True)
    parser.add_argument('-num_pr_bins', help='number of bins for sampling on the PR curve', type=int, default=1000)
    parser.add_argument('-iou_thrs', help='thresholds for evaluation', type=str, default='0.5')
    parser.add_argument('-splits', help='splits that need to evaluate', type=str, default='hard')
    parser.add_argument('-use_official_eval', action='store_true')
    parser.add_argument('-verbose', action='store_true')
    args = parser.parse_args()

    if args.iou_thrs == 'all':
        iou_thrs = [0.5 + 0.05 * i for i in range(10)]
    else:
        iou_thrs = sorted([float(t) for t in args.iou_thrs.split(',')])
    pr_info = evaluate(args.submission_dir, args.num_pr_bins, args.use_official_eval,
        args.verbose, splits=args.splits.split(','), iou_thrs=iou_thrs)

    summary = '\t\t\t%s\t   mAP\n'%('\t'.join(['  %.02f'%(v) for v in iou_thrs]))
    for pidx, split in enumerate(pr_info):
        summary += '%s\t\t'%(split)

        map_t = 0
        for iou in pr_info[split]:
            ap_t = pr_info[split][iou]['ap']
            map_t += ap_t
            summary += '\t%.04f'%(round(ap_t, 4))
        map_t = map_t / float(len(pr_info[split]))
        summary += '\t%.04f\n'%(round(map_t, 4))

        samples = pr_info[split][iou_thrs[0]]['samples']

        plt.plot([p[1] for p in samples], [p[0] for p in samples],
            label='%s[AP@%g:%.3f, mAP:%.3f]'%(
                split.rjust(6),
                iou_thrs[0],
                pr_info[split][iou_thrs[0]]['ap'],
                map_t))

    print(summary)

    plt.xlabel('recall', fontname='Courier New', fontsize=11)
    plt.ylabel('precision', fontname='Courier New', fontsize=11)

    plt.xticks([0.1 * t for t in range(1, 11)], family='Courier New')
    plt.yticks([0.1 * t for t in range(0, 11)], family='Courier New')

    plt.xlim(0, 1)
    plt.ylim(0, 1)
    plt.grid(True)

    # adjust the scale of axis
    plt.gca().set_aspect(1, adjustable='box')

    # set the legend
    legend = plt.legend(loc='lower left', frameon=True)
    plt.setp(legend.texts, family='Courier New', weight='bold')

    plt.show()
    plt.close()

